<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Gg telefonía la mejor opción para opción para tu empresa " /> 
    <meta name="author" content="Gg telefonía"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Gs telefonía">
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="Las mejores ventas para tí">
	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("icon-152x152.png")}}">
	<title>Gs telefonía celular</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link href="{{asset('css/adminlte.min.css')}}" rel="stylesheet">

    <!------ Include the above in your HEAD tag ---------->

		<!-- Global site tag (gtag.js) - Google Analytics -->
        <style>
            #main{
                background-repeat: no-repeat;
                display:block;
                position:fixed;
                width:100%;
                height:100%;
                font-family: Segoe UI;
                font-style: normal;
                font-weight: normal;
                color: rgb(4, 77, 214);
                   }
            .main-header{
            background-color: rgb(12, 8, 207);
            /* Full height */
        
            justify-content: center;
            /* Center and scale the image nicely */
            display: block;
            background-position: center;
           
            opacity: 0.8;
            overflow: visible;
            border: none;
    
            }
    .btn-login{
        white-space: nowrap;
            text-align: center;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 115%;
            background-color: #4300ff !important;
            border-width: 0;
            border-radius: 2px;
            box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
            transition: background-color .3s;
            overflow: hidden;
    
    }
    .btn-login:hover, .btn-login:focus {
      background-color: #200f50;
    }
    .rectangule-initial{
        position: fixed;
        top: 15%;
        bottom: 50%;
    }
  
 

.loader {
  position: fixed;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  width: 100vw;
  height: 100vh;
  z-index: 99999;
  background-color: #ffffff;
  opacity: 0.9;

}


.disabled{
  display: none;
}
.loading{
  opacity: 0.5;
}
.components a{
color: #11023a !important;
}
.components a:hover{
background-color: #4007dd !important;
}
.sidebar-dark-primary{
    background-color: #ffffff;
    border: none;
}
.menu-row{
    background-color: rgba(27, 9, 194, 0.67);
}
        </style>

	@yield('css')

</head><!--/head-->
<body class="hold-transition sidebar-mini " id="menu" >
  <div id="loader"  class="loader">
    <div class="text-center" style="position: relative !important; top: 25vh; ">
      <a href="#" class="text-center"><img src="{{asset('images/logo.png')}}"  style="width: 150px"></a>
      <h2 style="color:#a39d9d">Cargando....</h2>

    </div>  
  </div>

  <div class="wrapper" >
  <!-- Main Sidebar Container -->
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item mx-auto mt-2">
          <span class="text-light text-center" style="font-weight: 800; color: #ffffff;">Gs telefonía celular</span>
    </li>
      <li class="nav-item dropdown ">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-bell"></i>
          <span class="badge badge-warning navbar-badge"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">15 Notificaciones</span>
          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul>

 
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4 "  >
 

    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user panel (optional) -->
     

      <!-- Sidebar Menu -->
      <nav class="mt-2" >
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <div class="row mb-4 mt-4 text-white menu-row">
               
                <div class="col-8 col-xs-8 col-md-8">
                    <p class="text-center font-weight-bold" style="font-size: 100%">Menú</p>
                </div>
                <div class="col-3 col-xs-3 col-md-3">
                  <a class=" btn text-left" data-widget="pushmenu" style="background-color: transparent; width:100%; height:100%; border:none !important;" type="button" >
                <i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
                  </a>
              </div>
              </div>
            <ul class="list-unstyled components mb-5" >
                <li class="nav-item">
                  <a  class="nav-link text-center">
              </a>
              </li>
                  <li class="nav-item text-left">
                   <a >Usuario actual :{{Auth::user()->name}}</a>
                   </li>
                   <li class="nav-item text-left">
                    <a  class="nav-link" href="#"><i class="fa fa-home" aria-hidden="true"></i>
                        Inicio</a>                  
                  </li>
                  @if (Auth::user()->hasRole('admin'))
                  <li class="nav-item text-left">
                    <a  class="nav-link" href="#"><i class="fa fa-edit" aria-hidden="true"></i>
                        Productos</a>                  
                  </li>
                  @endif
               
                  <li class="nav-item text-left">
                    <a  class="nav-link" href="{{ route('admin.buys') }}"><i class="fa fa-money" aria-hidden="true"></i>
                        Ventas</a>                  
                  </li>
                  @if (Auth::user()->hasRole('admin'))
                  <li class="nav-item text-left">
                    <a  class="nav-link" href="{{ route('admin.users') }}"><i class="fa fa-user" aria-hidden="true"></i>
                        Usuarios</a>                  
                  </li>
                  @endif
                  <li class="nav-item text-left">
                    <a  class="nav-link" href="{{ route('password.request') }}"><i class="fa fa-key" aria-hidden="true"></i>
                        Cambiar contraseña</a>
                      </li>
                      <li class="nav-item">
                        <a  class="nav-link"> 
                          <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="customSwitch1">
                          <label class="custom-control-label" for="customSwitch1">Notificaciones</label>
                        </div> 
                      </a>
                        </li>
                   <li>
                   <form id="form_logout" action="{{route('logout')}}" method="POST">
                  @csrf
                  </form>
                       <a href="javascript:;" onclick="document.getElementById('form_logout').submit();" class="nav-link">Cerrar sesión</a>
                   </li>      
        </ul>
 
      </nav>
 
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
    <div class="text-center" style="position: relative !important; bottom: -150px;">
        <a href="#" class="text-center"><img src="{{asset('images/logo.png')}}"  style="width: 50%"></a>

    </div>  
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color: transparent;">
    <!-- Content Header (Page header) -->
   
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" >
      <div class="container-fluid"  >
        <div class="row" >
            @yield('content')

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


    </div>
		
	</div>
  <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> 
  <script type="text/javascript" src="{{asset('js/popper.js')}}"></script> 
  <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/adminlte.min.js')}}"></script>
  <script src="{{asset('js/custom.js')}}"></script>
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>

    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: "6f3f7aaf-7630-4573-9b79-a8d2209c8393",
      });
    });

  
$( document ).ready(function() {
  // Handler for .ready() called.
    $("#loader").hide()

});
$(window).on("beforeunload", function() { 
  $("#loader").show();
});

  </script> 


      <!-- end - carousel logos empresas -->  
  
    @yield('scripts')
    
</body>
</html>