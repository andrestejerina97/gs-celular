<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sending extends Model
{
    protected $guarded=['id','created_at','updated_at'];
    public function delivery()
    {
     return $this->belongsTo('App\User')->withDefault([
        'delivery' => 'No',
    ]);
    }
    public function invoice()
    {
     return $this->belongsTo('App\invoice');
    }
}
