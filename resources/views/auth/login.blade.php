@extends('layouts.app')
@section('content')
<form method="POST" id="form_user" action="{{route('login')}}" >
    @csrf
    <div id="usuario">           
        <div data-height="cover-header" class="caption">
            <div class="caption-center">
                <div class="left-30 right-30 bottom-20 ">
                    <h1>Para seguir, ingresá tu e-mail o usuario</h1>
                    <div class="input-style has-icon input-style-1 input-required">
                        <input type="name" name="emai" id="email" value="{{old('email') }}"  autocomplete="email" autofocus  required placeholder="E-mail o usuario">
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                    <div class="clear"></div>
                    <a href="javascript:;" id="btn_continue"class=" button button-full button-m button-round-small bg-highlight top-30 bottom-30">Continuar</a>
                <a href="{{route('register')}}" ><span class="color-blue2 center-text uppercase font-14">Crear cuenta</span></a>
                   
                <a href="{{ route('password.request')}}" class=" button button-full button-m button-round-small bg-gray1-light  top-30 bottom-30"><span class="color-black center-text uppercase font-14"><i class="fab fa-google color-black"></i>  Ingresar con Google</span></a>
                <div class="clear"></div>

                    
                </div>
            </div>
        </div>      
    </div>
    <div class="page-content" style="display: none" id="contrasena">
        <div data-height="cover-header" class="caption">
            <div class="caption-center">
                <div class="left-30 right-30 bottom-20 top-40">
                    <h1>Ahora, tu clave</h1>
                    <div class="input-style has-icon input-style-1 input-required">
                        <input type="password" name="password" required placeholder="Clave">
                    </div>        
                    <div class="clear"></div>
                    <a href="javascript:document.getElementById('form_user').submit()" class="button button-full button-m button-round-small bg-highlight top-30 bottom-30">Ingresar</a>    
                    <input class="form-check-input"  type="hidden" name="remember" id="remember" value="1">
                    <input type="hidden" name="email" id="email_aux">
                    @if (Route::has('password.request'))
                    <a href="{{ route('password.request')}}" ><span class="color-blue2 center-text uppercase font-14">No sé mi clave</span></a>
                @endif
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="clear"></div>
                </div>
            </div>
        </div>      
        <div class="caption-bg"></div>
    </div>
  
  
</form>  

@endsection
@section('scripts')
 <script>

     $(document).on('click','#btn_continue',function (e) {
         e.preventDefault();
         if ($("#email").val()!='') {
        $("#contrasena").css('display','block');
        $("#usuario").css('display','none');
        $("#email_aux").val($("#email").val());

    }else{
        $("#email").focus();
    }

     });
 
function loadPassword(e) {
    
}     
</script>   
@endsection