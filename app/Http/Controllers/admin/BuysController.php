<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\invoice;
use App\invoice_detail;
use App\Products;
use App\sending;
use App\status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BuysController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index()
    {
     if(Auth::user()->hasRole('admin')){
      $buys=invoice::with('user')->with('sending.delivery')->join('statuses','statuses.id','invoices.status')->select('invoices.*','statuses.name as status')->orderBy('created_at','desc')->get();
      return view('admin.buys.index')->with('buys',$buys);
     }
     if(Auth::user()->hasRole('delivery')){
      $buys=sending::where('delivery_id',Auth::user()->id)->with('invoice.detail')->orderBy('created_at','desc')->get();

   //   $buys=invoice::with('user')->with('sending.delivery')->join('statuses','statuses.id','invoices.status')->select('invoices.*','statuses.name as status')->orderBy('created_at','desc')->get();
      return view('admin.buys.delivery')->with('buys',$buys);
     }
    }

    public function edit($id=0)
    {
     if($id==0){
         $customers= User::orderBy('lastname','desc')->get();
         $products=Products::orderBy('name','desc')->get();
         $employees= User::role('delivery')->get();
         
         return view('admin.buys.edit',compact('employees','customers','products'));   
       }else{
        $customers= User::orderBy('lastname','desc')->get();
        $products=Products::orderBy('name','desc')->get();
        $employees= User::role('delivery')->get();
        $buy=invoice::with('user')->with('detail')->with('sending.delivery')->join('statuses','statuses.id','invoices.status')->select('invoices.*','statuses.name as status')->where('invoices.id',$id)->first();
        return view('admin.buys.edit',compact('buy','employees','customers','products'));   
       }
    }
    public function search(Request $request)
    {
        $datas=$request->except('_token');
        $users= User::Name($datas['name'])
        ->UserName($datas['username'])
        ->orderBy('id','DESC')->get();
      return view('admin.users.index',compact('users'));
    
   }
   public function searchuser($id,Request $request)
   {
     $users=User::join('users','users.users_id','users.id')
     ->where('publications_id','=',$id)
     ->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('users.id','DESC')
     ->with('users')->with('publications')
     ->get();
     return view('admin.users.index')
     ->with('users',$users)
     ->with('publications_id',$id);
   }
   public function update(Request $request)
   {
     $data= $request->except('_token','photo','role','password_confirmation','password');

     if ($request->input('password')=="") {
       $validatedData = $request->validate([
         'name' => ['required', 'string', 'max:255'],
         'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
         'role' => ['required','min:3'],
 
       ]);

     }else{

       $validatedData = $request->validate([
         'name' => ['required', 'string', 'max:255'],
         'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
         'password' => ['required', 'string', 'min:8', 'confirmed'],
         'role' => ['required','min:3'],
 
       ]);
       $data['password']=Hash::make($request->input('password'));
     }

       
         $user=User::where('id','=',$request->input('id'))->update($data);
         return response()->json(['result'=>$request->input('id')]);

   }

   public function store(Request $request)
   {
         $validatedData = $request->validate([
           'total' => ['required','integer','min:1'],
           'place_buy' => ['required', 'string'],
         ],
         [
           'total.integer' => "El total no puede ser 0",
         ]
       );
      
       if (!is_numeric($request->customer_id)) {
        $customer= User::create([
          'name'=>$request->customer_id,
          'cellphone'=>$request->cellphone
        ]);
        $customer->assignRole('user');
        $request->customer_id=$customer->id;
      }else{
        $customer=User::find($request->customer_id);
      }
            
      if (!is_numeric($request->product_id)) {
        $product= Products::create([
          'name'=>$request->input('product_id','No definido'),
          'offer_price'=>$request->total,
          'active'=>'no',

        ]);
        $request->product_id=$product->id;
      }else{
        $product=Products::find($request->product_id);
        $request->product_id=$product->id;
      }

      
       if($request->place_buy=='delivery' && $request->delivery_id != ''){
        $invoice=invoice::create([
          'name'=>$customer->name,
          'total'=>$request->total,
          'cellphone'=>$request->cellphone,
          'pay'=>$request->pay,
          'total_delivery'=>$request->total_delivery,
          'status'=> 1,
          'customer_id'=>$customer->id,
          'modified_id'=>Auth::user()->id,
          'creatur_id'=>Auth::user()->id,
                  ]);
          $sending= sending::create([
            'delivery_id'=>$request->delivery_id,
            'invoice_id'=>$invoice->id,
            'time_at'=>$request->time_at,
            'date'=>date('Y-m-d'),
            'status'=>1,
            'latitude'=>$request->lat,
            'longitude'=>$request->long,
          
          ]);
        

       }else{
        $invoice=invoice::create([
          'name'=>$customer->name,
          'total'=>$request->total,
          'cellphone'=>$request->cellphone,
          'pay'=>$request->pay,
          'total_delivery'=>0,
          'status'=> 2,
          'creatur_id'=>Auth::user()->id,
          'modified_id'=>Auth::user()->id,
          'customer_id'=>$customer->id,


        ]);
       }

       invoice_detail::create([
         'name'=>$product->name,
         'product_id'=>$product->id,
         'quantity'=>1,
         'price'=>$product->offer_price,
         'discount'=>0,
         'invoice_id'=>$invoice->id
       ]);
     
      


     return response()->json(['result'=>$invoice->id]);

   }
   public function delete($id){
     if ($id != 'id') {
       $user= invoice::find($id);
       $user->delete();
       return response()->json(['result'=>1]);
     }else{
       return response()->json(['result'=>-1]);

     }
   }
}
