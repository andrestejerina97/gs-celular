<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
   protected $table='products';
   protected $guarded=['id','created_at','updated_at'];
   public function colors()
   {
    return $this->hasMany('App\product_has_colors','product_id');
   }
   public function images()
   {
    return $this->hasMany('App\product_has_images','product_id');
   }
}
