<?php

use App\Categories;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
 use App\status;
class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category= new Categories();
        $category->category_name="Accesorios";
        $category->save();
        $category= new Categories();
        $category->category_name="Celulares";
        $category->save();

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);
        Role::create(['name' => 'supervisor']);
        Role::create(['name' => 'vendedor']);
        Role::create(['name' => 'delivery']);

        $user=User::create([
            'name' => "admin",
            'email' => "admin@gs.com",
            'password' => Hash::make("admin1234"),
        ]);
        $user->assignRole("admin");
        $user=User::create([
            'name' => "supervisor",
            'email' => "supervisor@gs.com",
            'password' => Hash::make("supervisor1234"),
        ]);
        $user->assignRole("supervisor");
        $user=User::create([
            'name' => "user",
            'email' => "user@gs.com",
            'password' => Hash::make("user1234"),
        ]);
        
        $user->assignRole("user");
        $user=User::create([
            'name' => "delivery",
            'email' => "delivery@gs.com",
            'password' => Hash::make("delivery1234"),
        ]);
        $user->assignRole("delivery");

        status::create([
            'name'=>'Enviado'
        ]);
        status::create([
            'name'=>'Pagado'
        ]);
        status::create([
            'name'=>'Cancelado'
        ]);
        }

     
}
