<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('delivery_id');
            $table->foreign('delivery_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('invoice_id');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade')->onUpdate('cascade');
            $table->string('time_at')->nullable();
            $table->string('time_end')->nullable();
            $table->string('date')->nullable();
            $table->text('observation')->nullable();
            $table->string('status',50)->nullable();
            $table->text('latitude')->nullable();
            $table->text('longitude')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('provider',50)->nullable();
            $table->string('photo',250)->nullable();
            $table->string('lastname',250)->nullable();
            $table->string('username',250)->nullable();
            $table->string('cellphone',250)->nullable();
            $table->string('home',250)->nullable();

            $table->integer('is_active')->nullable();
            $table->softDeletes();

          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendings');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('provider');
            $table->dropColumn('photo');
            $table->dropColumn('lastname');
            $table->dropColumn('username');
            $table->dropColumn('is_active');

            $table->dropSoftDeletes();

          });
    }
}
