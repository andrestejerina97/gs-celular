
@extends('layouts.admin')
@section('content')
    

<form id="form_add_product" method="POST" action="{{route('product.save')}}" accept-charset="UTF-8"
enctype="multipart/form-data">
@csrf
@include('admin.form-product')

</form>
@endsection