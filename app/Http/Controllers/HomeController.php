<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
       
            $categories=Categories::all();
            $products= Products::with('colors')->with('images')->where('stock','>',0)->inRandomOrder()->paginate(10);
            $destakes=Products::where('stock','>',0)->where('destake',1)->inRandomOrder()->paginate(10);
    
            return view('admin.index',compact('products','categories','destakes'));        
              }
        if ($user->hasRole('supervisor')) {
         return redirect()->route('supervisor');
         # code...
        }
        if ($user->hasRole('delivery')) {
            $categories=Categories::all();
            $products= Products::with('colors')->with('images')->where('stock','>',0)->inRandomOrder()->paginate(10);
            $destakes=Products::where('stock','>',0)->where('destake',1)->inRandomOrder()->paginate(10);
    
            return view('admin.index',compact('products','categories','destakes'));            # code...
           }
        if ($user->hasRole('user')) {
            $categories=Categories::all();
            $products= Products::with('colors')->with('images')->where('stock','>',0)->inRandomOrder()->paginate(10);
            $destakes=Products::where('stock','>',0)->where('destake',1)->inRandomOrder()->paginate(10);
    
            return view('welcome',compact('products','categories','destakes'));         
        }
       
    }
}
