@extends('layouts.public')
@section('content')
<div class="content">
    <h4>Elige como quieres recibir tu producto:</h4>
  </div>
<div class="content">
    
    <div class="link-list link-list-2 link-list-long-border">
        <a href="{{route('product.checkout',$id)}}">
            <i class=" 	fa fa-motorcycle color-dark2-light"></i>
            <span>Recibir el producto en mi  domicilio</span>
            <strong>El envío es sin cargo</strong>
        </a>
        <a href="">
            <i class="fas fa-child color-dark2-light"></i>
            <span>Comprar y retirar en nuestro local</span>
            <em class="bg-blue2-dark"></em>
            <strong>Con nuestra buena atención de siempre</strong>
        </a>
     
    </div>
</div>
   
@endsection
@section('scripts')
<script>

  function see_product(element) {
    let id=$(element).attr("id");
    sessionStorage.setItem('product_active',id)
    location.href="productos.html"
     }
  </script>    
@endsection