<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Products;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       // $products= Products::all();
        $products=Products::select(DB::raw("CONCAT('".url("storage/products")."/',products.avatar) as avatar"),'products.id','products.name','products.offer_price')->get();

      //  $posts =  json_encode(Post::select(array(DB::raw("CONCAT('http://localhost:8000/images/', image) AS imageurl")))->get(), JSON_UNESCAPED_SLASHES);
        //return view('admin.products',compact('products'));
        return response()->json($products, 200)
        ->setEncodingOptions(JSON_UNESCAPED_SLASHES);
    }
    public function show($id)
    {
       // $products= Products::all();
        $products=Products::select(DB::raw("CONCAT('".url("storage/products")."/',products.avatar) as avatar"),'products.id','products.name','products.offer_price')
        ->Where('products.id','=',$id)->get();

      //  $posts =  json_encode(Post::select(array(DB::raw("CONCAT('http://localhost:8000/images/', image) AS imageurl")))->get(), JSON_UNESCAPED_SLASHES);
        //return view('admin.products',compact('products'));
        return response()->json($products, 200)
        ->setEncodingOptions(JSON_UNESCAPED_SLASHES);
    }
    public function create()
    {
        $categories= Categories::all();
        return view('admin.create-product',compact('categories'));      
    }
    public function update()
    {
        # code...
    }
    public function save(Request $request)
    {
        $product= new products();
   
        $file = $request->file('avatar');
       // $currentUser = Auth::user();
        $product->name= $request->input('name');
        $product->code=$request->input('code');
        $product->category_id=$request->input('category');
        $product->description= $request->input('description');
        $product->purchase_price=$request->input('purchase_price');
        $product->price=$request->input('price');
        $product->offer_price=$request->input('offer_price');
        $product->destake=$request->input('destake');
        $product->stock=$request->input('stock');
        $product->bestseller=$request->input('bestseller');   
        $product->save();
        $product->avatar="product-".$product->id.".". $file->getClientOriginalExtension();
        $product->update();
                  
        $path = $file->storeAs('products',$product->avatar);

          //   Storage::disk('public')->put($product->avatar,  \File::get($file));

       //indicamos que queremos guardar un nuevo archivo en el disco local
       //Storage::disk('local')->put($nombre,  \File::get($file));
        return redirect()->route('product.index');
    }

    public function delete()
    {
        # code...
    }
}
