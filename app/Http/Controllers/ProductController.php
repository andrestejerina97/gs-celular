<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Http\Controllers\Controller;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Stripe\Product;

class ProductController extends Controller
{
    public function index()
    {
       // $products= Products::all();
        $products=Products::select(DB::raw("CONCAT('".url("storage/products")."/',products.avatar) as avatar"),'products.id','products.name','products.offer_price')->get();

      //  $posts =  json_encode(Post::select(array(DB::raw("CONCAT('http://localhost:8000/images/', image) AS imageurl")))->get(), JSON_UNESCAPED_SLASHES);
        //return view('admin.products',compact('products'));
        return response()->json($products, 200)
        ->setEncodingOptions(JSON_UNESCAPED_SLASHES);
    }
    public function show($id)
    {   
       // $products= Products::all();
        $products=Products::select(DB::raw("CONCAT('".url("storage/products")."/',products.avatar) as avatar"),'products.id','products.name','products.offer_price')
        ->Where('products.id','=',$id)->get();

      //  $posts =  json_encode(Post::select(array(DB::raw("CONCAT('http://localhost:8000/images/', image) AS imageurl")))->get(), JSON_UNESCAPED_SLASHES);
        //return view('admin.products',compact('products'));
        return response()->json($products, 200)
        ->setEncodingOptions(JSON_UNESCAPED_SLASHES);
    }
    public function create()
    {
        $categories= Categories::all();
        return view('admin.create-product',compact('categories'));      
    }
    public function update()
    {
        # code...
    }
    public function save(Request $request)
    {
        
        $product= new products();
   
        $file = $request->file('avatar');
       // $currentUser = Auth::user();
        $product->name= $request->input('name');
        $product->code=$request->input('code');
        $product->category_id=$request->input('category');
        $product->description= $request->input('description');
        $product->purchase_price=$request->input('purchase_price');
        $product->price=$request->input('price');
        $product->offer_price=$request->input('offer_price');
        $product->destake=$request->input('destake');
        $product->stock=$request->input('stock');
        $product->bestseller=$request->input('bestseller');   
        $product->save();
        $product->avatar="product-".$product->id.".". $file->getClientOriginalExtension();
        $product->update();
                  
        $path = $file->storeAs('products',$product->avatar);

          //   Storage::disk('public')->put($product->avatar,  \File::get($file));

       //indicamos que queremos guardar un nuevo archivo en el disco local
       //Storage::disk('local')->put($nombre,  \File::get($file));
        return redirect()->route('product.index');
    }

    public function viewId($id=0)
    {
        if($id > 0){
            $products=Products::where('id','=',$id)->get();
            return view("public.products.view",compact('products'));
        }else{
            //return view("public.products.view",compact('products'));

        }
    }
    public function buy($id=0)
    {
        if($id > 0){
            $id=$id;
            return view("public.products.buy",compact('id'));
        }
    }
    public function checkout($id=0)
    {
        if($id > 0){
            $id=$id;
            return view("public.products.checkout")
            ->with('id',$id);
        }
    }
    public function getOrder($id)
    {
        $product=Products::find($id);
        $total= (float) (0.0 + $product->precio);
        $preferenceData = [
            'items' => [
                [
                    'id' => $id,
                    'category_id' => 'productes',
                    'title' => "Desafío enfocate, product ".$product->name,
                    'description' => 'Desafío enfocate, construí tu cuerpo perfecto por medio de la adquisición de tu product '.$product->nombre,
                    'picture_url' => '',
                    'quantity' => 1,
                    'currency_id' => 'ARS',
                    'unit_price' => $total
                ]
            ],
            "back_urls" => array(
                "success" => route('collections.success',["medio"=>1,"id"=>$id]),
                "failure" => route('collections.fails',["medio"=>1,"id"=>$id]),
                "pending" => route('collections.pending',["medio"=>1,"id"=>$id]),
            ),
        ];

       
       // $preference = MP::create_preference($preferenceData);
        //$preference= $preference['response']['sandbox_init_point'];
       

        // Si no existe mostramos error.
      
            $preference = MP::create_preference($preferenceData);
  
            $preference= $preference['response']['sandbox_init_point'];
    
            
            return view('products.buy-product')
            ->with('mercadopago',$preference);
    }
}
