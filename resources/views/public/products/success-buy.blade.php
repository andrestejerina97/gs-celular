@extends('layouts.public')
@section('content')

<div class="content">
    <h2>Compra registrada con éxito!</h2>

    <h2>En unos momentos saldrá tu pedido!</h2>

</div>
  
@endsection
@section('scripts')
<script>
    function add_favorite(element) {
    let favorite_element_count=0;
       favorite_element_count=0+localStorage.getItem('favorite_element_count');
  

    let id=$(element).attr("id");
    $(element).find('i').removeClass("color-teal-dark");
    let favorite_element_count_new=1+parseInt(favorite_element_count);
    localStorage.setItem('favorite_element_count',favorite_element_count_new);
    localStorage.setItem('favorite_element_id_'+favorite_element_count_new,id);

  }
  function see_product(element) {
    let id=$(element).attr("id");
    sessionStorage.setItem('product_active',id)
    location.href="productos.html"
     }
  </script>    
@endsection