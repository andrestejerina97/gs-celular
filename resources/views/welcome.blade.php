@extends('layouts.public')
@section('content')
    <!-- Carousel-->
    <div class="content bottom-0">
      <h5 class="bold">Lo más vendido</h5>
    </div>
    <div class="content">

      <div class="double-slider owl-carousel owl-no-dots">
                @foreach ($products as $product)        
      <div id="more_destake">
              <div class="caption">
               
              <img width="100px"  style="min-height: 150px;max-height: 150px" class="caption-image owl-lazy" 
              @isset($product->avatar)
              data-src="{{asset('storage/products/'.$product->avatar)}}"
              @else
              data-src="{{asset('products/images/products/kolor.jpeg')}}"
              @endisset
              >
              </div>
      </div>
    
      @endforeach
    </div>
  
      <a href="#" class="prev-slide round-large bg-highlight"><i class="fa fa-angle-left"></i></a>
      <a href="#" class="next-slide round-large bg-highlight"><i class="fa fa-angle-right"></i></a>
    </div>
    <div class="divider divider-margins"></div>
<!--End carousel-->
<!--Categories-->

<div class="content content-center">
  <p>Categorías:</p>
  <a href="#" class="icon icon-xs icon-circle bg-gray2-light"><i class="fas fa-headphones-alt color-dark2-dark"></i></a>
  <a href="#" class="icon icon-xs icon-circle bg-gray2-light"><i class=" 	fas fa-mobile-alt color-dark2-dark"></i></a>
  <a href="#" class="icon icon-xs icon-circle bg-gray2-light"><i class=" 	fas fa-wifi color-dark2-dark"></i></a>
  <a href="#" class="icon icon-xs icon-circle bg-gray2-light"><i class=" 	fas fa-mobile color-dark2-dark"></i></a>
  <a href="#" class="icon icon-xs icon-circle bg-gray2-light"><i class=" fas fa-diagnoses color-dark2-dark"></i></a>

</div>
<div class="divider"></div>
<div class="content">
  <p>Vistos recientemente</p>
</div>

<div class="clear"></div>

@foreach ($products as $product)        
<div class="content">
  <div class="pricing-1 round-medium bg-white">

  <a class="add_favorite center-text" onclick="add_favorite(this);" id="{{$product->id}}" href="javascript:;">
    <i class="fa fa-heart color-teal-dark"></i><span >Agregar a favoritos</span></a>
    <div class="" id="{{$product->id}}" onclick="see_product(this)" >
      <div class="single-slider owl-carousel owl-dots-under bottom-0 owl-loaded owl-drag">           
        <img {{asset('storage/products/'.$product->avatar)}} class="caption-image owl-lazy" src="{{asset('storage/products/'.$product->avatar)}}" class="preload-image responsive-image bottom-15" alt="img">
        <img {{asset('storage/products/'.$product->avatar)}} class="caption-image owl-lazy" src="{{asset('storage/products/'.$product->avatar)}}" class="preload-image responsive-image bottom-15" alt="img">

      </div>
  </div>
  <div class="clear"></div>
  <ul class="pricing-list center-text bottom-30">
  <li><h1 class="bold font-700"><span>$</span>{{$product->offer_price}}</h1></li>
  <li><h2 class=""> {{$product->name}}</h2></li>
  <li><span><i class="far fa-money-bill-alt color-dark1-light" aria-hidden="true"></i> Recibí el producto en tu hogar</span>  </li>
  <li><h1><span class="bold font-700" id="offer_price"></span></h1> </li>

</ul>
        <h5 class="font-600 color-blue2-dark center-text"><i class="fa fa-motorcycle color-blue2-dark"></i> Envío gratis</h5>

</div>
</div>
@endforeach



@endsection
@section('scripts')
<script>
    function add_favorite(element) {
    let favorite_element_count=0;
       favorite_element_count=0+localStorage.getItem('favorite_element_count');
  

    let id=$(element).attr("id");
    $(element).find('i').removeClass("color-teal-dark");
    let favorite_element_count_new=1+parseInt(favorite_element_count);
    localStorage.setItem('favorite_element_count',favorite_element_count_new);
    localStorage.setItem('favorite_element_id_'+favorite_element_count_new,id);

  }
  function see_product(element) {
    let id=$(element).attr("id");
    sessionStorage.setItem('product_active',id)
    url="{{route('product.view',-1)}}"
    url= url.replace('-1',id);
    location.href= url;
     }
  </script>    
@endsection