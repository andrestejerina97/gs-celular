<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('price')->nullable();
            $table->float('purchase_price')->nullable();
            $table->float('comercial_house')->nullable();
            $table->float('offer_price')->nullable();
            $table->text('description')->nullable();
            $table->string('code')->nullable();
            $table->string('active')->nullable();

            $table->string('stock')->nullable();
            $table->string('destake')->nullable();
            $table->string('bestseller')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('avatar')->nullable();
            $table->timestamps();
        });
        Schema::table('products', function($table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
