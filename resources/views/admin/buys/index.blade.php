
@extends('layouts.admin')
@section('content')
	<div class=" col-lg-12 col-md-12 col-12 col-sm-12">
<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Ventas</h4>
  </div>
  <div class="card-body ">
 
  <form class="form-horizontal" method="get" action="{{route('admin.search.buy')}}" role="form">
    @csrf

  <div class="form-group">
    <div class="col-lg-2">
      <label for="inputEmail1" class="col-lg-2 control-label">-</label>
    </div>
    <div class="col-lg-6">
      <select id="service_id" name="service_id" class="form-control" >
    </div>
  </div>
  <div class="form-group">
  <div class="col-lg-4">
		<div class="input-group">
		  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		  <input type="date" name="date_at" value="" class="form-control" placeholder="Palabra clave">
		</div>
    </div>
    <div class="col-lg-2">
    	<button class="btn  btn-secondary" style="margin: 0px 0px 20px 0px">Buscar</button>
    </div>
  </div>
</form>
       
<div class="form-group text-center">
    <a href="{{route('admin.edit.buy')}}" class="btn btn-primary text-center m-4">+ Nueva Venta</a>

</div>
<div class="table-responsive">

            @if (count($buys)>0)
			<table class="table table-bordered table-hover">
			<thead>
        <th>Fecha</th>
            <th>Cliente</th>
			<th>Total</th>
      <th>Cobro</th>
      <th>Delivery</th>

      <th>Estado</th>

      <th>Creador</th>

      <th></th>

			</thead>
            @foreach($buys as $buy)
            <tr>
            <td>{{$buy->created_at}}</td>
            <td>{{$buy->name}}</td>
            <td>${{$buy->total}}</td>
            <td>{{$buy->pay}}</td>
            <td>@if ($buy->total_delivery ==0)
                NO
                @else
                {{$buy->sending[0]->delivery->name}}
            @endif</td>
            <td>{{$buy->status}}</td>
            <td>{{$buy->user->name}}</td>

            <td style="width:280px;">
            <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
           {{--<a href="{{route('admin.edit.buy',$buy->id)}}" class="btn btn-warning btn-xs">Editar</a> --}}
            <a href="javascript:;" onclick="eliminar(this);" data-id_buy={{$buy->id}} class="btn btn-danger btn-xs">Eliminar</a>
            </td>
            </tr>
            @endforeach
            @else
            <p class='alert alert-danger'>No hay Ventas</p>
            @endif
            </table>
        </div>

			</div>
			</div>

	</div>
@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>
    function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar esto?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_buy=$(a).data("id_buy");
               let url="{{route('admin.delete.buy','id')}}"
               url=url.replace("id",id_buy);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"registro eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.buys")}}';


                      });

                    }else{

                    }
               
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

    </script>    
@endsection


