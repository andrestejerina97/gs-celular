
  <!--formulario para nuevo producto-->
  <section id="form_section" class="col-lg-10 col-md-10 col-xs-10 col-10 mx-auto">
      <div class="form-row">
        <div class="form-group col-md">
            <label for="">Nombre del producto:</label>
            <input type="text" name="name" id="name" tabindex="1" class="form-control" 
            placeholder="ingrese nombre completo" 
            @isset($product->name)
            {{$product->name}}
            @endisset
            required>
        </div>
  
        <div class="form-group col-md">
          <label for="">Casa comercial:</label>
            <input pattern="[A-Za-z]{0,20}"  type="text" name="comercial_house" tabindex="2" id="comercial_house" class="form-control"
                placeholder="ingrese una descripción para mostrar al público"
                @isset($product->comercial_house)
                {{$product->comercial_house}}
                @endisset
                >
           
        </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md">
          <label for="">Descripción del producto:</label>
          <input type="text" name="description" id="description" tabindex="1" class="form-control" 
          placeholder="ingrese descripción (opcional)"
          @isset($product->description)
          {{$product->description}}
          @endisset
           >
      </div>
  </div>
    <div class="form-row">
        <div class="form-group col-md">
            <label for="">Precio de compra(opcional):</label>
            <input type="text" pattern="^(\d{1}\.)?(\d+\.?)+(,\d{2})?$"  title="El punto separa decimales y la coma los miles"  name="purchase_price" tabindex="3" id="purchase_price" class="form-control"
                placeholder="0.00"
                @isset($product->purchase_price)
                {{$product->purchase_price}}
                @endisset
                >
        </div>
        <div class="form-group col-md">
          <label class="" for="">Precio de venta normal:</label>                           
          <input type="text" tabindex="4" id="price" name="price" class="form-control"
              placeholder="0.00" pattern="^(\d{1}\.)?(\d+\.?)+(,\d{2})?$"  title="El punto separa decimales y la coma los miles"
              @isset($product->price)
              {{$product->price}}
              @endisset
              required>
        </div>
        <div class="form-group col-md">
          <label class="" for="">Precio de venta oferta:</label>                           
          <input type="text" tabindex="5" id="offer_price" name="offer_price" class="form-control"
              placeholder="0.00" pattern="^(\d{1}\.)?(\d+\.?)+(,\d{2})?$"  title="El punto separa decimales y la coma los miles"
              @isset($product->offer_price)
              {{$product->offer_price}}
              @endisset
              required>
        </div>
    </div>
  
    <div class="form-row">
        <div class="form-group col-md">
            <label for="">Stock de producto(opcional):</label>
            <input type="tel" tabindex="5" name="stock" id="stock" class="form-control" value="1" placeholder="Cantidad de unidades disponibles" pattern="[0-9]{1,4}"  title="el stock deben ser 3 números" 
            @isset($product->stock)
            {{$product->stock}}
            @endisset
            required>
        </div>
        <div class="form-group col-md">
          <label for="">Codigo de producto:</label>
          <input type="tel" tabindex="6" name="code" id="code" class="form-control" placeholder="Codigo que identificará el producto"
          @isset($product->code)
          {{$product->code}}
          @endisset
          >
      </div>
      
  </div>
  <div class="form-row">
  <div class="input-group  col-md ">
  <label class="form-label" for="">Categoría :&nbsp;</label>
  <select class="form-control " id="category" name="category">
    @foreach ($categories as $category)
  
  <option  value="{{$category->id}}"   
    @isset($product->category_name)
    @if($product->category()->id ==$category_id)
    selected
    @endif
    @endisset
    >{{$category->category_name}}</option>
    @endforeach
  </select>
  </div>  
  </div>
  <div class="form-row">
  
      <div class="form-group col-md">
        <label for="">Imagen de venta(obligatorio):</label>
        <input type="file" class="form-control" id="avatar" name="avatar" required >  
    </div>
  
  </div>
  <div class="form-row">
    <div class="form-group col-md">
      <label for="">¿Destacado?:</label>
      <select class="form-control " id="destake" name="destake">
        <option  value="si"
        @isset($product->destake)
        @if($product->destake =='si')
        selected
        @endif
        @endisset
        >SI</option>
        <option  value="no"
        @isset($product->destake)
        @if($product->destake =='no')
        selected
        @endif
        @endisset>NO</option>
      </select>
    </div>
    <div class="form-group col-md">
      <label for="">Más vendido?:</label>
      <select class="form-control " id="bestseller" name="bestseller">
        <option  value="si"
        @isset($product->bestseller)
        @if($product->bestseller =='si')
        selected
        @endif
        @endisset
        >SI</option>
        <option  value="no"
        @isset($product->bestseller)
        @if($product->bestseller =='no')
        selected
        @endif
        @endisset>NO</option>
      </select>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md">
      <label for="">Ingrese Excel de productos</label>
  
      <input type="file" id="excel" class="form-control" name="excel"  >  
  </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md text-right">
      <button type="button" id="btn_cancel_new_customer" class="btn btn-danger">Cancelar</button>
        <button type="submit" class="btn btn-success">Guardar</button>
    </div>
  
  </div>    
    </section>