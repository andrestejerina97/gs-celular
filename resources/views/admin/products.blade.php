@extends('layouts.admin')
@section('content')


<div class="row">
    <div class="col-lg-10 col-md-10 col-xs-10 col-10 mx-auto">
      <hr class="my-4">
      <div id="message"></div>
    </div>
    <section id="table_customer_section" class="col-lg-10 col-md-10 col-xs-10 col-10 mx-auto">
       
  
      <form method="post" action="{{route('product.search')}}" >
        <div class="row">
  
        <div class="col-lg-6 col-md-6 col-6">
          <div class="input-group">
                <label for="select_filter">Filtro</label>
                <select class="form-control" name="select_filter" id="select_filter">
                  <option value="dni">Nombre</option>
                  <option value="lastname">Código</option>
                </select>
              </div>
        </div>
        <div class="col-lg-6 col-md-6 col-6">
          <div class="form-inline">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="text" class="form-control " name="customer_filter" id="customer_filter" required>
                <button  type="submit" class="btn btn-success"><i class="fa fa-search">Buscar</i></button>
            </div>  
        </div>
      </div>
  
      </form> 
    <table class="table table-hover table-responsive">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Precio Compra</th>
            <th>Precio Oferta</th>
            <th>Stock</th>
            <th>Código</th>
            <th>Destacado</th>
            <th>Mas vendido</th>
            <th></th>
          </tr>
        </thead>
        <tbody id="body_table_product">
          @foreach ($products as $product)
          <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->description}}</td>
            <td>{{$product->price}}</td>
            <td>{{$product->purchase_price}}</td>
            <td>{{$product->offer_price}}</td>
            <td>{{$product->stock}}</td>
            <td>{{$product->code}}</td>
            <td>{{$product->destake}}</td>
            <td>{{$product->bestseller}}</td>
            <td>
              <button id_med="{{$product->id}}" type="button " id="btn_update" class="btn btn-success btn_update"><i class="ti-pencil-alt"></i></button>  
          </td>
          <td>
            <form method="post" action="{{route('product.delete')}}" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" id="id" value="{{$product->id}}">
                <button  type="submit" class="btn btn-success"><i class="ti-trash"></i></button>
              </form>
            </td>
            <td>
          </tr>
          @endforeach
            
  
        </tbody>
      </table>
  <div class="col-lg-12">
  <div class="pagination ">
  
  </div>
      </div>
      <div class="row">
        <div class="col-lg-12" >
            <div class="text-right">
            <a class="btn btn-primary" href="{{route('product.create')}}">Nuevo Producto</a>
            </div>
            </div>
      </div>
  </section>
  </div>
  
@endsection