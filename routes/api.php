<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('product', 'productController@index');
Route::get('product/{product}', 'productController@show');
Route::post('product', 'productController@store');
Route::post('get-plan', 'productController@consultarPlan');
Route::put('product/{product}', 'productController@update');
Route::delete('product/{product}', 'productController@delete');
 
