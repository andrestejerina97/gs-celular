<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class customer extends Model
{
    use HasRoles;
      protected $guarded=['id','created_at','updated_at'];
}
