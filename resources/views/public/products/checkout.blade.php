@extends('layouts.public')
@section('content')
<div class="content">
    <div class="content">
        <h4>Confirma tu ubicación para que podamos hacer la entrega:</h4>
        <button onclick="usar_ubicación();" class="button button-xs bg-blue2-dark button-center-large button-circle bottom-2 uppercase" >Usar mi ubicación actual</button>
      </div>
    <div class="content">
        <div  style="width:100%; height:100%;" id="map" class="map-full map">
            <div data-height="cover-header" class="caption ">
                <div class="caption-center">
                </div>
                <div class="caption-overlay"></div>
            </div>          
        </div>
 

      <div class="clear"></div>
      </div>
      <div id="ubicacion_manual" class="content">
        <h5 class="bold">O ingresa tu dirección aquí:</h5>
        <p>
        </p>

        <div class="input-style has-icon input-style-1 input-required">
            <i class="input-icon  	fas fa-compass "></i>
            <span>Calle principal</span>
            <input type="principal_street" placeholder="Calle principal...">
        </div> 
        <div class="input-style has-icon input-style-1 input-required">
          <i class="input-icon  fas fa-map-marked-alt"></i>
          <span>Número</span>
          <input type="house_number" placeholder="Número...">
      </div> 
      <div class="input-style has-icon input-style-1 input-required">
        <i class="input-icon  	  	far fa-building"></i>
        <span>Piso/N de departamento</span>
        <input type="departament_number" placeholder="Piso/N de departamento...">
    </div> 
        <div class="input-style has-icon input-style-1 input-required">
          <i class="input-icon  	fas fa-compass"></i>
          <span>Calle Secundaria</span>
          <input type="secondary_street" placeholder="Calle secundaria...">
      </div>        
    </div>

  <input type="hidden" id="url_buy" value="{{route('product.get.order',$id)}}">

      <div  class="link-list link-list-2 link-list-long-border" >
        <a id="btn_link_pago" class="purchase_active_order" href="#">
            <i class=" 	far fa-credit-card color-gray2-light"></i>
            <span id="total_to_pay"></span>
            <em class="bg-blue2-dark">PAGAR AHORA</em>
            <strong>Recibir a domicilio</strong>
        </a>
       
    </div>
</div>
@endsection
@section('scripts')
<script>
function usar_ubicación() {
  $("#ubicacion_manual:input").attr("disabled",'true');
}
  </script>    
@endsection