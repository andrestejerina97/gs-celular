

  <div class="form-group">

    <label for="inputEmail1" class="col-lg-2 control-label">Nombre y Apellido:</label>

    <div class="col-lg-10" >
      <select id="customer" name="customer_id" class=" select2 form-control" style="width: 100%" required>
        <option value="">-- SELECCIONE --</option>
        @foreach ($customers as $customer)
        <option  value="{{$customer->id}}"
         @isset($buy->customer_id)
        @if ($buy->customer_id == $customer->id)
            selected
        @endif
        @endisset
          >{{$customer->name}} {{$customer->lastname}}</option>         
        @endforeach
      </select>
    </div>
</div>
  
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Celular:</label>
    <div class="col-lg-10" >
     <input type="text" name="cellphone" class="form-control" value="@isset($buy)
     {{$buy->cellphone}}
     @endisset">
    </div>
</div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Producto:</label>
    <div class="col-lg-10">
      <select id="product_id"  name="product_id" class="select2 form-control " style="width: 100%" required>
        <option value="">-- SELECCIONE --</option>
        @foreach ($products as $product)
        <option  data-amount="{{$product->offer_price}}" value="{{$product->id}}"
         @isset($buy->detail[0]->product_id)
        @if ($buy->detail[0]->product_id == $product->id)
            selected
        @endif
        @endisset
          >{{$product->name}}</option>         
        @endforeach
          
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Neto a cobrar:</label>
    <div class="col-lg-10" >
     <input type="text" name="total" class="form-control" value="@isset($buy->total)
     {{$buy->total}}
     @endisset">
    </div>
</div>
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Método de pago:</label>
    <div class="col-lg-10">

      <select name="pay" class="form-control" required>
        <option value="efectivo" @isset($buy->pay) @if ($buy->pay=='efectivo') selected @endif @endisset>Efectivo</option>
        <option value="mercado_pago"  @isset($buy->pay) @if ($buy->pay=='mercado_pago')selected @endif @endisset>Mercado Pago</option>
        <option value="tarjeta"  @isset($buy->pay) @if ($buy->pay=='tarjeta') selected @endif @endisset>Tarjeta</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">¿Lugar de venta?:</label>
    <div class="col-lg-10">
      <select name="place_buy" id="place_buy" class="form-control" required>
        <option value="local"  @isset($buy->pay) @if ($buy->total_delivery==0) selected @endif @endisset >Local</option>
        <option value="delivery"@isset($buy->pay) @if ($buy->total_delivery != 0) selected @endif @else selected @endisset >Delivery</option>

      </select>
    </div>
  </div>


<div id="div_with_delivery">
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-2 control-label">Costo del delivery($):</label>
        <div class="col-lg-10" >
         <input type="text" name="total_delivery" placeholder="Ej: 1200.00" class="form-control" value="@isset($buy->total_delivery)
         {{$buy->total_delivery}}
         @endisset">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-2 control-label">Delivery:</label>
        <div class="col-lg-10">
          <select name="delivery_id" class="form-control" >
            <option value="">-- SELECCIONE --</option>
            @foreach ($employees as $employee)
            <option  value="{{$employee->id}}"
             @isset($buy->sending[0]->delivery_id)
            @if ($buy->sending[0]->delivery_id == $employee->id)
                selected
            @endif
            @endisset
              >{{$employee->name}}</option>         
            @endforeach
              
          </select>
        </div>
      </div>
    
    
      <div class="form-group">
        <label for="inputEmail1" class="col-lg-2 control-label">Hora de salida</label>
        <div class="col-md-12">
          <input type="time" id="time_at" value="@isset($buy->sending[0]->time_at){{date('H:i',strtotime($buy->sending[0]->time_at))}}@endisset" name="time_at"  class="form-control" placeholder="Hora">
        </div>
      </div>
    
      <div class="form-group">
        <label for="inputEmail1" class="col-lg-2 control-label">Ubicación</label>
    
        <div class="col-lg-10">
            <div id="map"></div>
    
        </div>
      </div>
</div>
<input type="hidden" id="lat" name="lat">
<input type="hidden" id="long" name="long">

 <!-- <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Asunto</label>
    <div class="col-lg-10">
      <input type="text" name="title" value="" required class="form-control" id="inputEmail1" placeholder="Asunto">
    </div>
  </div> -->
