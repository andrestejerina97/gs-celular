<!DOCTYPE HTML>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Gs-Telefonía</title>
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="{{asset('_manifest.json')}}" data-pwa-version="set_by_pwa.js">
<link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('styles/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/framework.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('fonts/css/fontawesome-all.min.css')}}"> 
    <style media="screen">
      body { background: #ECEFF1; color: rgba(0,0,0,0.87); font-family: Roboto, Helvetica, Arial, sans-serif; margin: 0; padding: 0; }
      #message { background: white; max-width: 360px; margin: 100px auto 16px; padding: 32px 24px; border-radius: 3px; }
      #message h2 { color: #ffa100; font-weight: bold; font-size: 16px; margin: 0 0 8px; }
      #message h1 { font-size: 22px; font-weight: 300; color: rgba(0,0,0,0.6); margin: 0 0 16px;}
      #message p { line-height: 140%; margin: 16px 0 24px; font-size: 14px; }
      #message a { display: block; text-align: center; background: #039be5; text-transform: uppercase; text-decoration: none; color: white; padding: 16px; border-radius: 4px; }
      #message, #message a { box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24); }
      #load { color: rgba(0,0,0,0.4); text-align: center; font-size: 13px; }
      @media (max-width: 600px) {
        body, #message { margin-top: 0; background: white; box-shadow: none; }
        body { border-top: 16px solid #ffa100; }
      }
    </style>
  </head>
     
<body class="theme-light" data-highlight="blue2">
        
  <div id="page">
    
    <div id="page-preloader">
        <div class="loader-main"><div class="preload-spinner border-highlight"></div></div>
    </div>
      <div class="header ">
      </div>
      <div class="menu-hider"></div>

    <div class="page-content ">           
 @yield('content')

</div>
<div class="clear"></div>

<div class="divider divider-margins"></div>

<p class="offline-message bg-red2-dark color-white center-text uppercase ultrabold">No hay conexión a internet</p>
<p class="online-message bg-green1-dark color-white center-text uppercase ultrabold">Estás en linea nuevamente. Bienvenido!</p>

<a href="#" class="back-to-top-icon back-to-top-icon-circle bg-highlight"><i class="fa fa-angle-up"></i></a>
<div class="clear"></div>

</div>

<script type="text/javascript" src="{{asset('scripts/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/customMap.js')}}" ></script>
@yield('scripts')
  </body>
</html>
