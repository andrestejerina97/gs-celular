<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Products;
use Illuminate\Http\Request;
use MP;
class PaymentController extends Controller
{
    public function GenerateSingleBuy($id=0)
    {
     if ($id >0) {
        $product= Products::find($id);
        $total= (float) (0.0 + $product->offer_price);
    
        $preferenceData = [
            'items' => [
                [
                    'id' => $id,
                    'category_id' => 'Productos GS',
                    'title' => $product->name,
                    'description' => "Gs:".$product->name,
                    'quantity' => 1,
                    'currency_id' => 'ARS',
                    'unit_price' => $total
                ]
            ],
            "back_urls" => array(
                "success" => route('collections.success',["medio"=>1,"id"=>$id]),
                "failure" => route('collections.fails',["medio"=>1,"id"=>$id]),
                "pending" => route('collections.pending',["medio"=>1,"id"=>$id]),
            ),
        ];

       
       // $preference = MP::create_preference($preferenceData);
        //$preference= $preference['response']['sandbox_init_point'];
       

        // Si no existe mostramos error.
       //dd($preferenceData);
            $preference = MP::create_preference($preferenceData);
  
            $preference= $preference['response']['sandbox_init_point'];
    
            
            return response()->json(['link_pago'=>$preference,'total'=>$total])->setEncodingOptions(JSON_UNESCAPED_SLASHES);
            //('products.buy-product')
            //->with('mercadopago',$preference);

     }
    }
    public function success()
    {
        return view("public.products.success-buy");

    }
    public function fail()
    {
        
    }
    public function pending()
    {
        
    }
}
