<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
Route::get('install', function() {

    Artisan::call('migrate:refresh',['--force'=>true,'--seed'=>true]);

});
*/





Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/ver-producto/{id?}', 'ProductController@viewId')->name('product.view');
Route::get('/comprar-producto/{id?}', 'ProductController@buy')->name('product.buy');
Route::get('/checkout-producto/{id?}', 'ProductController@checkout')->name('product.checkout');

Route::get('/get-order/{id?}', 'PaymentController@GenerateSingleBuy')->name('product.get.order');

//partials date
Route::get('/guardar-orden/{id?}', 'PurchaseOrderController@purchase')->name('purchase.order.single');

//mp
Route::get('/compra-exitosa', 'PaymentController@success')->name('collections.success');
Route::get('/compra-fallida', 'PaymentController@fail')->name('collections.fails');
Route::get('/compra-cancelar', 'PaymentController@pending')->name('collections.pending');

//fin mp

Route::namespace('admin')->prefix('/admin')->name('admin.')->group(function(){
    Route::get('/admin', 'ProductsController@index')->name('product.index');

    Route::get('/nuevo-producto', 'ProductsController@create')->name('product.create');
    Route::post('/actualizar-producto', 'ProductsController@update')->name('product.update');
    Route::post('/guardar-producto', 'ProductsController@save')->name('product.save');
    Route::post('/buscar-producto', 'ProductsController@search')->name('product.search');
    Route::post('/eliminar-producto', 'ProductsController@delete')->name('product.delete');

    /**Usuarios */
        Route::get('/usuarios','UsersController@index')->name('users');
        Route::get('/mi-usuario/{id?}','UsersController@edit')->name('edit.user');
        Route::post('/guardar-usuario-/{id?}','UsersController@store')->name('store.user');
        Route::get('/eliminar-usuario/{id?}','UsersController@delete')->name('delete.user');
        Route::get('/servicio-usuario','UsersController@search')->name('search.user');
        Route::get('/home', 'HomeController@index')->name('home');
    
        /**Ventas */
        Route::get('/ventas','BuysController@index')->name('buys');
        Route::get('/mi-venta/{id?}','BuysController@edit')->name('edit.buy');
        Route::post('/guardar-venta-/{id?}','BuysController@store')->name('store.buy');
        Route::get('/eliminar-venta/{id?}','BuysController@delete')->name('delete.buy');
        Route::post('/buscar-venta', 'BuysController@search')->name('search.buy');

});





Auth::routes();
