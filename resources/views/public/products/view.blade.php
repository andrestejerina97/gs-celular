@extends('layouts.public')
@section('content')
    @foreach ($products as $product)
        
   <div class=" pricing-4 round-medium bg-gray1-light">
    <div class="single-slider owl-carousel owl-dots-under bottom-0 owl-loaded owl-drag">

  <div id="product_section">
  <img class="caption-image owl-lazy" data-src="{{asset('storage/products/'.$product->avatar)}}" alt="galaxy s20" >

      </div>    </div>
      <ul class="pricing-list center-text bottom-30">
      <li><h4 class="bold font-900 " id="brand">{{$product->name}}</h4></li>
      <li> <span  id="description">{{$product->description}}</span></li>
        <li><span><i class="fas fa-money-check-alt color-dark1-light"></i> Pagá hasta 3 cuotas sin interés</span>  </li>

        <li><span><i class="fa fa-motorcycle color-dark1-light"></i> Envio a domicilio gratis</span>  </li>
        <li><span><i class="	far fa-money-bill-alt color-dark1-light" aria-hidden="true"></i> Comprá y retirá en nuestro local</span>  </li>
      <li><h1 class="bold font-900 " id="offer_price">${{$product->offer_price}}</h1> </li>
      <li>
          <a href="#" data-dropdown="dropdown-4" class="dropdown-style-2 bg-grey1-light round-tiny">
            <i class="fas fa-dot-circle color-black"></i>  
            <p class="color-black">Colores disponibles</p>
              <i class="fa dropdown-icon fa-angle-down color-black"></i>
          </a>
          <div class="dropdown-content bottom-0" id="dropdown-4">
              <div class="link-list link-list-1 link-list-no-border top-10">
                
                <div class="fac fac-checkbox-round "><span style="color: hsl(145, 73%, 45%)"></span>
                <input id="box1-fac-checkbox-round" type="checkbox" value="1" checked="">
                <label for="box1-fac-checkbox-round">Simple</label>
            </div>  
            <div class="fac fac-checkbox-round "><span style="color: hsl(145, 73%, 45%)"></span>
              <input id="box2-fac-checkbox-round" type="checkbox" value="1" checked="">
              <label for="box1-fac-checkbox-round">Simple</label>
          </div>  
              </div>
              <div class="divider top-20 bottom-0"></div>
          </div>     
    
      </li>
      <li>
        <li>
          <a href="#" data-dropdown="dropdown-5" class="dropdown-style-2 bg-grey1-light round-tiny center-text">
            <p class=" color-black">Cantidad: <span id="text_quantity"></span></p>
              <i class="fa dropdown-icon fa-angle-down color-black"></i>
          </a>
          <div class="dropdown-content bottom-0" id="dropdown-5">
              <div class="link-list link-list-1 link-list-no-border top-10">
             <a href=""><span>1 unidad</span></a>
             <a href=""><span>2 unidades</span></a>
             <a href=""><span>3 unidades</span></a>
             <a href=""><span>4 unidades</span></a>
             <a href=""><span>5 unidades</span></a> 
              </div>
              <div class="divider top-20 bottom-0"></div>
          </div>     
    
      </li>
      </li>
      </ul>
      
      <a href="javascript:;" onclick="buy_product(this)" data-product={{$product->id}} class="button button-xs bg-blue2-dark button-center-large button-circle bottom-2 uppercase">Comprar ahora</a>
      
      <a href="#" class="button button-xs bg-gray2-dark button-center-large button-circle top-20 uppercase">
      <i class="fas fa-shopping-cart" aria-hidden="true"></i> Agregar al carrito</a>
    
    </div>
    <br><br>

    <div class="content center-text">
      <p>
      </p>
      <a href="#" class="shareToFacebook icon icon-xs icon-round bg-facebook regularbold"><i class="fab fa-facebook-f"></i></a>
      <a href="#" class="shareToTwitter icon icon-xs icon-round bg-twitter regularbold"><i class="fab fa-twitter"></i></a>
      <a href="#" class="shareToWhatsApp icon icon-xs icon-round bg-whatsapp regularbold"><i class="fab fa-whatsapp"></i></a>
      <a href="#" class="shareToMail icon icon-xs icon-round bg-mail regularbold"><i class="fa fa-envelope"></i></a>
  </div>
  <div class="content accordion-style-2">     
    <a href="#" data-accordion="accordion-content-5">
      Descripción del producto
        <i class="accordion-icon-right fa fa-angle-down"></i>
    </a>
    <p id="accordion-content-5" class="accordion-content bottom-10">
        This is the accordion content. You can add any content you want to it. Really, anything!
        Add images, text, lists, captions or any element you want.
    </p>
  </div>
  
  <div class="content accordion-style-2">     
      <a href="#" data-accordion="accordion-content-5">
          Características del producto
          <i class="accordion-icon-right fa fa-angle-down"></i>
      </a>
      <p id="accordion-content-5" class="accordion-content bottom-10">
          This is the accordion content. You can add any content you want to it. Really, anything!
          Add images, text, lists, captions or any element you want.
      </p>
</div>


@endforeach

  


@endsection
@section('scripts')
<script>
      
function buy_product(a) {
  let id=$(a).data("product");
  url="{{route('purchase.order.single',0)}}";
  url=url.replace('0',id)
  $.ajax({
        url: url,
        method:"get",
        dataType: "json",
        beforeSend:function(){
          $('.loader-main').addClass('loader-inactive');

        },
        success:function(response)
        {    
          
            if (response.result == 1) {
              url2='{{route("product.buy",0)}}';
              url2=url2.replace(0,response.id);
        location.href=url2;
               
                }
         },
        error:function(request,errorType,errorMessage) {
            console.log(errorMessage);
            $('.loader-main').removeClass('loader-inactive');

        }
        });
}
  function see_product(element) {
    let id=$(element).attr("id");
    sessionStorage.setItem('product_active',id)
    location.href="productos.html"
     }
  </script>    
@endsection