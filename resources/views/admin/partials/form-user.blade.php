@isset($user->photo)
<div class="form-group text-center">
    <label for="password-confirm" class="text-center">Foto actual:</label>

    <div class="">

   <img loading="lazy" class="text-center" src="{{asset('storage/users/'.$user->id."/".$user->photo)}}"  style="max-width: 50px; min-width:50px; max-height: 50px;    border-radius: 50%;
   "  id="photo" style=""  >  
        </div>
    </div>
@endisset
     
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
    <div class="col-md-6">
      <input type="text" name="name" value="@isset($user) {{$user->name}} @endisset" class="form-control" id="name" placeholder="Nombre">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Apellido*</label>
    <div class="col-md-6">
      <input type="text" name="lastname" value="@isset($user){{$user->lastname}}@endisset" required class="form-control" id="lastname" placeholder="Apellido">
    </div>
  </div>
  <div class="form-group" hidden>
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre de usuario*</label>
    <div class="col-md-6">
      <input type="text" hidden name="username"  value="@isset($user){{$user->username}}@endisset" class="form-control" id="username" placeholder="Nombre de usuario">
    </div>
  </div>


  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Email de acceso*</label>
    <div class="col-md-6">
      <input type="email" name="email" value="@isset($user){{$user->email}} @endisset" class="form-control" id="email" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Contrase&ntilde;a</label>
    <div class="col-md-6">
      <input type="password" name="password" class="form-control" id="inputEmail1" placeholder="Contrase&ntilde;a">
      @isset($user->password)
      <br>
      <div class="input-field">
        <p class="help-block">La contrase&ntilde;a solo se modificara si escribes algo, en caso contrario no se modifica.</p>
      </div>
      @endisset
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Confirmar Contrase&ntilde;a</label>
    <div class="col-md-6">
      <input type="password" name="password_confirmation" class="form-control" id="inputEmail1" placeholder="Confirmar Contrase&ntilde;a">
<p class="help-block">La contrase&ntilde;a solo se modificara si escribes algo, en caso contrario no se modifica.</p>
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label" ></label>
    <div class="col-md-6 col-lg-6">
    <label>
      Esta activo
      <input type="checkbox" name="is_active" @isset($user)@if($user->is_active)checked @endif @endisset> 
    </label>
  </div>
  </div>


  <div class="form-group">
    <label for="name" class="">Rol</label>

    <div class="">
        <select class="form-control" name="role" id="role" required>
            @isset($user->roles)
            <option >Seleccionar</option>
            <option value="admin" @if ($user->hasRole('admin')== true) selected @endif>Administrador</option>
            <option value="delivery" @if ($user->hasRole('delivery')== true) selected @endif>Delivery</option>
            <option value="user" @if ($user->hasRole('user')== true) selected @endif>Cliente</option>
            <option value="vendedor" @if ($user->hasRole('vendedor')== true)selected @endif>Vendedor</option>
            @else
            <option selected>Seleccionar</option>
            <option value="admin" >Administrador</option>
            <option value="delivery" >Delivery</option>
            <option value="vendedor" >Vendedor</option>
            <option value="user">Cliente</option>
            @endisset

        </select>
      
    </div>
</div>

<div class="form-group">
  <label for="password" class="">Foto de perfil</label>

  <div class="">
      <input id="photo" type="file" class="form-control @error('photo') is-invalid @enderror" name="photo" >

      @error('photo')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
  </div>
</div>