@extends('layouts.admin')
@section('css')
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>
   <style>
        /* Set the size of the div element that contains the map */
        #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }

   </style>
@endsection
@section('content')
    <div class="col-md-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                @isset($users)
                <h4 class="title">Editar Venta</h4>
                @else
                <h4 class="title">Nueva Venta</h4>

                @endisset
            </div>
            <div class="card-content table-responsive">
                <form class="form-horizontal" method="post" onsubmit="save(event)"  id="form_buy" "
                    role="form">
                    @csrf
                    @isset($users)
                    @foreach ($users as $user)
            <input type="hidden" name="id" value="{{$user->id}}">
                    @include("admin.partials.form-user")
    
                    @endforeach
                    @else
                 @include("admin.partials.form-buy")

                @endisset
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                         
                        @isset($users)
                        <button type="submit" class="btn btn-primary">Actualizar Venta</button>
                        @else
                        <button type="submit" class="btn btn-primary">Guardar Venta</button>
                    
                        @endisset
                        </div>
                      </div>

                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<script src="{{asset('js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/es.js')}}" type="text/javascript"></script>
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmTjC5yZKTVWz9w2YR88A1wNRAIy2iFIM&callback=initMap&libraries=&v=weekly"
  defer
></script>

<script>
   var lat='';
   var lng='';
   var marker='';
$(function(){
  @isset($buy->sending[0]->latitude)
  var coord={
    lat: {{$buy->sending[0]->latitude}},
    lng: {{$buy->sending[0]->longitude}}
  }
  @else
  var coord={
    
    lat: -29.423385,
    lng: -66.866722
  }
  @endisset
  var map = new google.maps.Map(document.getElementById('map'), {
    center: coord,
    zoom:15
  });
   marker = new google.maps.Marker({
    position:coord, 
    map: map,
    draggable: true
  });

  var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
  google.maps.event.addListener(searchBox,'places_changed',function(){
    var places = searchBox.getPlaces();
    var bounds = new google.maps.LatLngBounds();
    var i,place;
    for(i=0; place=places[i]; i++){
      bounds.extend(place.geometry.location);
      marker.setPosition(place.geometry.location);
    }
    map.fitBounds(bounds);
    map.setZoom(15);
  });

  google.maps.event.addListener(marker,'position_changed',function(){
   lat = marker.getPosition().lat();
     lng = marker.getPosition().lng();
    $('#lat').val(lat);
    $('#long').val(lng);
    alert(lng)
    
  });
});

$("#place_buy").change(function() {
  if(this.value=='local'){
    $("#div_with_delivery").hide();

  }else{
    $("#div_with_delivery").show();
  }
});

    </script>

<script>
    $('.select2').select2({
  language: "es",
  tags: true,
  theme: "classic"
});

    function save(e) {
      e.preventDefault();

      lat = marker.getPosition().lat();
     lng = marker.getPosition().lng();
        $('#lat').val(lat);
    $('#long').val(lng);
        var formdata = new FormData($("#form_buy")[0]);
            $.ajax({
                url         : "{{route('admin.store.buy')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Cambios guardados con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                        //let url="{{route('admin.edit.user',['id'=>0])}}"
                        //url=url.replace('0',data.result);
                        location.href='{!! url()->previous()!!}';//url;
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {

                      console.log(data.responseJSON.errors);
                      let lista= "";
                for(var k in data.responseJSON.errors) {
                lista += ""+ data.responseJSON.errors[k][0] +"";              
                }
                
              lista+="";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error: '+lista,
                      icon: 'error',
                    });                    }
                  }
            });
    }
</script>
@endsection